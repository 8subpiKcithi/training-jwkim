
var tui = require('tui-code-snippet');
var CONST = require('./constant.js');
var util = require('./util.js');

var DomHandler = tui.defineClass({

    /**
     * initialize
     * @param {Object} domElements - event target elements
     */
    init: function(domElements) {
        var chkDomElement = [];
        tui.forEachOwnProperties(domElements, function(node, nodeName) {
            if (tui.inArray(nodeName, CONST.DOM_ELEMENTS) > -1) {
                chkDomElement.push(nodeName);
            }
        });
        if (chkDomElement.length < CONST.DOM_ELEMENTS.length) {
            throw new Error('needDomElement');
        }
        tui.extend(this, domElements);
    },
    /**
     * print count info
     * @param {number} uncompleteCount - left tode count
     * @param {number} completeCount - completed count
     */
    printInfoCountElement: function(uncompleteCount, completeCount) {
        this.uncompleteCount.innerHTML = uncompleteCount;
        this.completeCount.innerHTML = completeCount;
    },
    /**
     * count print
     * @param {Array.<TodoItem>} todoList - todolist Array
     */
    printTodoListElement: function(todoList) {
        var listArrayLength = todoList.length;
        var htmlStringBuffer = [];
        var todo = null;
        var i = 0;
        var checked = '';
        var liHtml = [
            '<li class="{$checked}">',
            '<label>',
            '<input type="checkbox" name="seq" value="{$seq}" {$checked} />',
            '</label>',
            '<span>{$todoname}</span>',
            '</li>'
        ].join('');

        for (; i < listArrayLength; i += 1) {
            todo = todoList[i];
            checked = (todo.complete) ? 'checked' : '';

            htmlStringBuffer.push(util.template(liHtml, {
                'seq': todo.seq,
                'checked': checked,
                'todoname': todo.todoname
            }));
        }
        this.list.innerHTML = htmlStringBuffer.join('\n');
    },
    /**
     * filter type display (all, completed, uncompleted)
     * @param {number} filterType - filterType (0: ALL, 1: COMPLETE, 2: UNCOMPLETE )
     */
    printFilterElement: function(filterType) {
        var spanNode = this.filterButton.querySelectorAll('span');
        var length = spanNode.length;
        var i = 0;
        for (; i < length; i += 1) {
            util.removeClass(spanNode[i], 'active');
            if (Number(spanNode[i].getAttribute('data-filtertype')) === filterType) {
                util.addClass(spanNode[i], 'active');
            }
        }
    },
    /**
     * input area enter key event handler
     * @param {Function} callback - Callback after event handling.
     */
    onInputKeyDownHandler: function(callback) {
        util.addEventHandler(this.input, 'keydown', function(event) {
            var eventTarget = util.findEventTarget(event);
            event = event || window.event;
            if (event.keyCode === CONST.ENTER_KEYCODE) {
                if (callback) {
                    callback(eventTarget.value);
                }
                eventTarget.value = '';
            }
        });
    },
    /**
     * todolist checkbox event
     * @param {Function} callback - Callback after event handling.
     */
    onTodoCheckBoxClickHandler: function(callback) {
        util.addEventHandler(this.list, 'click', function(event) {
            var eventTarget = util.findEventTarget(event);
            if (eventTarget.tagName === 'INPUT') {
                if (callback) {
                    callback(eventTarget.value);
                }
            }
        });
    },
    /**
     * todo remove button event handler
     * @param {Function} callback - Callback after event handling.
     */
    onRemoveButtonClickHandler: function(callback) {
        util.addEventHandler(this.removeTodoButton, 'click', function() {
            if (callback) {
                callback();
            }
        });
    },
    /**
     * filter state change event handler
     * @param {Function} callback - Callback after event handling.
     */
    onFilterChangeButtonClickHandler: function(callback) {
        var template = [
            '<span data-filtertype="0" class="active">all</span>',
            '<span data-filtertype="2">active</span>',
            '<span data-filtertype="1">completed</span>'
        ].join('');
        this.filterButton.innerHTML = template;
        util.addEventHandler(this.filterButton, 'click', tui.bind(function(event) {
            var eventTarget = util.findEventTarget(event);
            var newFilterType = Number(eventTarget.getAttribute('data-filtertype'));
            if (callback) {
                callback(newFilterType);
            }
        }, this));
    }
});

module.exports = DomHandler;

